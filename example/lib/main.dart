import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_protobuf_plugin/generated/models.pb.dart';
import 'package:flutter_protobuf_plugin/generated/timestamp.pb.dart';
import 'package:flutter_protobuf_plugin/flutter_protobuf_plugin.dart';

void main() {
  runApp(ProtobufExampleApp());
}

class ProtobufExampleApp extends StatefulWidget {
  @override
  _ProtobufExampleAppState createState() => _ProtobufExampleAppState();
}

class _ProtobufExampleAppState extends State<ProtobufExampleApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Protocol Buffer in Plugin'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FutureBuilder<ExampleData>(
                future: FlutterProtobuf.sharedInstance.protoTest(ExampleData.create()
                  ..displayName = 'Protobuf Test data'
                  ..enum_2 = ExampleEnum.FAILURE
                  ..issueDate = new Timestamp(nanos: 1000000)),
                builder: (context, snapshot) {
                  return Center(
                    child: Text('Message that went from Flutter to Android converted and got back'
                        '\n${snapshot.data?.displayName}'
                        '\n${snapshot.data?.enum_2}'
                        '\n${snapshot.data?.issueDate}'),
                  );
                }),
            FutureBuilder<Map<dynamic, dynamic>>(
                future: FlutterProtobuf.sharedInstance.nonProtoTest({
                  'displayName': 'Protobuf Test data',
                  'enum': 1,
                  'issueDate': {
                    "nanos": 1000000,
                  }
                }),
                builder: (context, snapshot) {
                  return Center(
                    child: Text('Message that went from Flutter to Android converted and got back'
                      '\n${snapshot.data["displayName"]}'
                      '\n${snapshot.data["enum"]}'
                      '\nseconds: ${snapshot.data["issueDate"]["seconds"]}'
                      '\nnanos: ${snapshot.data["issueDate"]["nanos"]}'),
                  );
                }),
          ],
        ),
      ),
    );
  }
}

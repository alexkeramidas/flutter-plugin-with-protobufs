# flutter_protobuf_plugin

Example flutter plugin with protobuf usage

## Getting Started

To generate the protobuf models you need to set them up for each platform first (ubuntu / macos / win).

Then

For Android its a plugin of android studio but you have to setup the projectRoot/android/build.gradle first.

For Flutter its `flutter pub global activate protoc_plugin` and then run
 
 `protoc --plugin=protoc-gen-dart=$HOME/.pub-cache/bin/protoc-gen-dart --dart_out=lib/generated -Iprotos protos/*.proto`

For iOS its the proto swift package

Follow the respective installation procedures and then generate the models with

Remove the file protos/timestamp.proto 

Change the timestamp reference in protos/models.proto to `import "google/protobuf/timestamp.proto";`

`protoc --swift_out=./ios/Classes  --plugin=protoc-gen-dart=$HOME/.pub-cache/bin/protoc-gen-dart ./protos/*.proto`

import Flutter
import UIKit
import SwiftProtobuf

public class SwiftFlutterProtobufPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_protobuf_plugin/methods", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterProtobufPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
            if(call.method == "protoTest"){
                do{
                    let incomingBytes: FlutterStandardTypedData = call.arguments as! FlutterStandardTypedData
                    let serializedBytes = Data(incomingBytes.data)
                    let decoded = try ExampleData.init(serializedData: serializedBytes)
                    let outgoingData = ExampleData.with {
                        $0.displayName = "Converted " + decoded.displayName
                        $0.enum = ExampleEnum.success
                        $0.issueDate = Google_Protobuf_Timestamp(seconds: 150, nanos: 15)
                    }
                    result(try outgoingData.serializedData())
                } catch {
                    result(FlutterError(code: "Cannot serialize data", message: nil, details: nil))
                }
            }
  }
}

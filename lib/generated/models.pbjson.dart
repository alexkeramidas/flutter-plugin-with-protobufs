///
//  Generated code. Do not modify.
//  source: models.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use exampleEnumDescriptor instead')
const ExampleEnum$json = const {
  '1': 'ExampleEnum',
  '2': const [
    const {'1': 'SUCCESS', '2': 0},
    const {'1': 'FAILURE', '2': 1},
  ],
};

/// Descriptor for `ExampleEnum`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List exampleEnumDescriptor = $convert.base64Decode('CgtFeGFtcGxlRW51bRILCgdTVUNDRVNTEAASCwoHRkFJTFVSRRAB');
@$core.Deprecated('Use exampleDataDescriptor instead')
const ExampleData$json = const {
  '1': 'ExampleData',
  '2': const [
    const {'1': 'displayName', '3': 1, '4': 1, '5': 9, '10': 'displayName'},
    const {'1': 'enum', '3': 2, '4': 1, '5': 14, '6': '.ExampleEnum', '10': 'enum'},
    const {'1': 'issueDate', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'issueDate'},
  ],
};

/// Descriptor for `ExampleData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List exampleDataDescriptor = $convert.base64Decode('CgtFeGFtcGxlRGF0YRIgCgtkaXNwbGF5TmFtZRgBIAEoCVILZGlzcGxheU5hbWUSIAoEZW51bRgCIAEoDjIMLkV4YW1wbGVFbnVtUgRlbnVtEjgKCWlzc3VlRGF0ZRgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSCWlzc3VlRGF0ZQ==');
@$core.Deprecated('Use exampleDataListDescriptor instead')
const ExampleDataList$json = const {
  '1': 'ExampleDataList',
  '2': const [
    const {'1': 'virtualKeys', '3': 1, '4': 3, '5': 11, '6': '.ExampleData', '10': 'virtualKeys'},
  ],
};

/// Descriptor for `ExampleDataList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List exampleDataListDescriptor = $convert.base64Decode('Cg9FeGFtcGxlRGF0YUxpc3QSLgoLdmlydHVhbEtleXMYASADKAsyDC5FeGFtcGxlRGF0YVILdmlydHVhbEtleXM=');

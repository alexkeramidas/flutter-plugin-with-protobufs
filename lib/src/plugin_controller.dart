import 'dart:typed_data';

import 'package:flutter_protobuf_plugin/generated/models.pb.dart';
import 'package:flutter_protobuf_plugin/src/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

abstract class DeviceController {
  Future<ExampleData> protoTest(ExampleData arg);
  Future<Map<dynamic, dynamic>> nonProtoTest(Map<dynamic, dynamic> arg);
}

class PluginController implements DeviceController {
  final MethodChannel _baseMethodChannel;

  PluginController({@required MethodChannel baseMethodChannel})
      : assert(baseMethodChannel != null),
        _baseMethodChannel = baseMethodChannel;

  @override
  Future<ExampleData> protoTest(ExampleData arg) async {
    final Uint8List rawData =
        await _baseMethodChannel.invokeMethod('protoTest', arg.writeToBuffer());
    return ExampleData.fromBuffer(rawData);
  }

  @override
  Future<Map<dynamic, dynamic>> nonProtoTest(Map<dynamic, dynamic> arg) async {
    return await _baseMethodChannel.invokeMethod('nonProtoTest', arg);
  }
}

class PluginControllerFactory {
  const PluginControllerFactory();

  PluginController create() {
    const _bleMethodChannel = MethodChannel(Constants.PLUGIN_NAMESPACE + "/methods");

    return PluginController(baseMethodChannel: _bleMethodChannel);
  }
}

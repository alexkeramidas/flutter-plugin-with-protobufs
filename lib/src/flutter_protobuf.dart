import 'package:flutter_protobuf_plugin/generated/models.pb.dart';
import 'package:flutter_protobuf_plugin/src/plugin_controller.dart';

class FlutterProtobuf {
  static final FlutterProtobuf sharedInstance = FlutterProtobuf._();

  factory FlutterProtobuf() => sharedInstance;

  FlutterProtobuf._() {
    print('initializing');
  }

  PluginController _pluginController;

  Future<void> initialize() async {
    _pluginController ??= PluginControllerFactory().create();
  }

  Future<ExampleData> protoTest(ExampleData arg) async {
    await initialize();

    return _pluginController.protoTest(arg);
  }

  Future<Map<dynamic, dynamic>> nonProtoTest(Map<dynamic, dynamic> arg) async {
    await initialize();

    return _pluginController.nonProtoTest(arg);
  }
}

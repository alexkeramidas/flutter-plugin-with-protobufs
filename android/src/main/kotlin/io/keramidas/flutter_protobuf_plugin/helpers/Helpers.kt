package io.keramidas.flutter_protobuf_plugin.helpers

import io.flutter.plugin.common.MethodCall

val MethodCall.bytes: ByteArray get() = arguments as ByteArray

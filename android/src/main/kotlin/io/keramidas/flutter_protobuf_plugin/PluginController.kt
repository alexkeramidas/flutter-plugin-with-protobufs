import android.content.Context
import com.google.protobuf.Timestamp
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel.Result
import io.keramidas.fluter_protobuf_plugin.Models
import io.keramidas.flutter_protobuf_plugin.helpers.bytes

class PluginController {
    /**
     * Plugin init method.
     */
    internal fun initialize(messenger: BinaryMessenger, context: Context) {

    }

    internal fun execute(call: MethodCall, result: Result) {
        methods[call.method]?.invoke(call, result) ?: result.notImplemented()
    }

    /**
     * Map of plugin methods that can be invoked from an application
     */
    private val methods = mapOf<String, (call: MethodCall, result: Result) -> Unit>(
            "protoTest" to this::protoTest,
            "nonProtoTest" to this::nonProtoTest
    )

    private fun protoTest(call: MethodCall, result: Result) {
        val data = Models.ExampleData.parseFrom(call.bytes)
        val successData = Models.ExampleData.newBuilder()
                .setDisplayName("Converted ${data.displayName}")
                .setEnum(if (data.enumValue == 1) Models.ExampleEnum.FAILURE else Models.ExampleEnum.SUCCESS)
                .setIssueDate(
                        Timestamp
                                .newBuilder()
                                .setNanos(data.issueDate.nanos + 1000)
                                .setSeconds(150)
                ).build()

        return result.success(successData.toByteArray())
    }

    private fun nonProtoTest(call: MethodCall, result: Result) {
        var displayName = call.argument<String>("displayName")
        val enum = call.argument<Int>("enum")
        val timestamp = call.argument<Map<String, Int>>("issueDate")

        displayName = "Converted $displayName"
        val returnEnum = if (enum == 1) "FAILURE" else "SUCCESS"
        val returnTimeStamp = mapOf("seconds" to 150, "nanos" to (timestamp?.get("nanos")!! + 1000))

        return result.success(
                mapOf(
                        "displayName" to displayName,
                        "enum" to returnEnum,
                        "issueDate" to returnTimeStamp)
        )
    }
}
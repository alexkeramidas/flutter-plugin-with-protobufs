package io.keramidas.flutter_protobuf_plugin

import PluginController
import android.content.Context
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry
import io.keramidas.flutter_protobuf_plugin.constants.Constants

/** FlutterProtobufPlugin */
class FlutterProtobufPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {

    /**
     * Plugin API V2 Plugin registration.
     */
    override fun onAttachedToEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        initializePlugin(binding.binaryMessenger, binding.applicationContext)
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {

    }

    companion object {
        lateinit var pluginController: PluginController

        /**
         * Plugin API V1 Plugin registration.
         * this enables support for apps that are using the legacy API
         */
        @JvmStatic
        fun registerWith(registrar: PluginRegistry.Registrar) {
            initializePlugin(registrar.messenger(), registrar.activeContext())
        }

        @JvmStatic
        private fun initializePlugin(messenger: BinaryMessenger, context: Context) {
            val methodChannel = MethodChannel(messenger, Constants.PLUGIN_NAMESPACE + "/methods")
            methodChannel.setMethodCallHandler(FlutterProtobufPlugin())
            pluginController = PluginController()
            pluginController.initialize(messenger, context)
        }
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        TODO("Not yet implemented")
    }

    override fun onDetachedFromActivityForConfigChanges() {
        TODO("Not yet implemented")
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        TODO("Not yet implemented")
    }

    override fun onDetachedFromActivity() {
        TODO("Not yet implemented")
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        pluginController.execute(call, result);
    }
}
